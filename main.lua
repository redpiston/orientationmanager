local orientationManger = nil

if (system.getInfo("platform") == 'android') then
	orientationManger = require("plugin.orientationManager")
end

if (orientationManger) then
	--set the orientation to portrait only
	orientationManger.setOrientation("portrait")
end

local function createFile(videoID)
	local path = system.pathForFile( "story.html", system.TemporaryDirectory )
		
	local fh, errStr = io.open( path, "w" )
		
	if fh then
				print( "Created file" )
				fh:write("<!doctype html>\n<html>\n<head>\n")
				fh:write("<style type=\"text/css\">\n html { -webkit-text-size-adjust: none; font-family: HelveticaNeue-Light, Helvetica, Droid-Sans, Arial, san-serif; font-size: 1.0em; } h1 {font-size:1.25em;} p {font-size:0.9em; } body, iframe {margin:0px;}</style>")
				fh:write("<meta name=\"viewport\" content=\"user-scalable=no\">")
				fh:write("</head>\n<body>\n")
		
				local height = display.pixelWidth*(10/16)
				fh:write('<iframe width="100%" height="' .. height .. '" src="https://www.youtube.com/embed/' .. videoID .. '?html5=1" frameborder="0" allowfullscreen></iframe>') --allowfullscreen
		
				fh:write( "\n</body>\n</html>\n" )
				io.close( fh )
			else
				print( "Create file failed!" )
			end
end

--sample corona video
createFile("AVcKdr6ODpM")
	
local bg = display.newRect(display.contentCenterX, display.contentCenterY, display.contentWidth, display.contentHeight)
bg:setFillColor(0.5)

local openBtn = display.newRect(display.contentCenterX, display.contentCenterY, 100, 50)
openBtn:setFillColor(0,1,0)

openBtn:addEventListener('tap', function()

	local group = display.newGroup()
	group.x, group.y = display.contentCenterX, display.contentCenterY
	
	local bg = display.newRect(0,0, display.contentWidth, display.contentHeight)
	bg:setFillColor(0.8)
	group:insert(bg)
	
	local closeBtn = display.newRect(0, -display.contentHeight*0.5+50, 100, 50)
	closeBtn:setFillColor(1,0,0)
	
	closeBtn:addEventListener('tap', function()
		if (orientationManger) then
			--set the orientation to portrait only
			orientationManger.setOrientation("portrait")
		end
		group:removeSelf()
	end)
	group:insert(closeBtn)
	
	if (orientationManger) then
		--set the orientation to unspecified
		orientationManger.setOrientation("all")
	end

	local webView = native.newWebView(0, 0, display.contentWidth*0.5, display.contentHeight*0.5)
	webView:request("story.html", system.TemporaryDirectory)
	
	group:insert(webView)
end)