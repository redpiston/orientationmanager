# OrientationManager

v1.0

This plugin is designed for dynamic orientation control on Android. Orientation Manager allows you to set the orientations of the corona activity dynamically if your app only supports orientations for specific screens.

Add to Build Settings:

plugins = {
	
	["plugin.orientationManager"] =
     {
     	publisherId = "com.redpiston",
		supportedPlatforms = { android = true }
     },
}

...

//set it to support all orientations by default
orientation =
{
	default = "portrait",
	supported = { "portrait", "portraitUpsideDown", "landscapeLeft", "landscapeRight" }
},



Example:

local orientationManger = require("plugin.orientationManager")

//set the application to only allow portrait mode
orientationManger.setOrientation("portrait")

//set the application back to all orientations after 10 seconds
timer.performWithDelay(10000, function()
	orientationManger.setOrientation("all")
end)


This plugin was created because I had an application that was built ONLY for portrait, however when the user would show a video in full screen and rotate the device the video could not be shown in landscape. This plugin allowed me to limit the orientation at runtime.


Orientation Modes Supported at this time:
-all
-portrait
-landscape